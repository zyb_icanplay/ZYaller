# ZYaller

> ZYaller 传承一生一点一滴

![ZYaller](https://raw.githubusercontent.com/ZYallers/ZYaller/master/static/image/password.jpg)

记录整理和分享学习生活工作中遇到的各种有价值有用和问题的精华文章。丰富阅历，开阔视野，记录成长过程点滴！

## 标签
- [PHP](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/php)
- [JavaScript](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/javascript)
- [Html&Css](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/html-css)
- [Linux](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/linux)
- [MySQL](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/mysql)
- [Redis](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/redis)
- [MongoDB](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/mongodb)
- [Git](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/git)
- [MacBook](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/macbook)
- [高并发](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/high-concurrent)
- [程序员](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/programmer)
- [网络安全](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/network-safety)
- [Chrome](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/chrome)
- [Golang](http://git.oschina.net/zyb_icanplay/ZYaller/tree/master/tag/golang)

## 开发
拉取本项目代码，在dev分支下提交您的code，审阅通过之后会合并到master分支，就是这么简单！

## 联系方式
- `QQ`：1308565859
- `微信`：zyb_icanplay
- `Email`：zyb_icanplay@163.com